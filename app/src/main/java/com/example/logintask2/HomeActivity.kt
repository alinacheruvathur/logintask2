package com.example.logintask2

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_home.*

class HomeActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)


        logoutHomeActivityButton.setOnClickListener {
            startActivity(Intent(this, MainActivity::class.java))
            SharedPreference.getInstance(applicationContext).clear()
        }

    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        when (item.itemId) {

            R.id.AboutID -> {
                Toast.makeText(this, "About selected", Toast.LENGTH_SHORT).show()
            }
            R.id.SettingsID -> {
                Toast.makeText(this, "Settings selected", Toast.LENGTH_SHORT).show()
            }
            R.id.logoutID -> {
                val intent = Intent(applicationContext, MainActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                startActivity(intent)
                SharedPreference.getInstance(applicationContext).clear()
            }
        }

        return super.onOptionsItemSelected(item)
    }
}