package com.example.logintask2

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import com.example.logintask2.LoginResponse
import kotlinx.android.synthetic.main.activity_main.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class MainActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        progressBarbaseactivity.visibility = View.GONE

        LoginbtnLoginbutton.setOnClickListener {
            val  email = EmailLogineditText.text.toString().trim()
            val password = PasswordLogineditText.text.toString().trim()
            val user_type: Int = 1

            if (email.isEmpty()){
                EmailLogineditText.error = "Email required"
                EmailLogineditText.requestFocus()
                return@setOnClickListener

            }

            if (password.isEmpty()){
                PasswordLogineditText.error = "Password required"
                PasswordLogineditText.requestFocus()
                return@setOnClickListener


            }

            showLoading()

            if (isNetworkConnected()) {
                RetrofitClient.instance.login(email, password, user_type)
                    .enqueue(object : Callback<LoginResponse> {
                        override fun onResponse(
                            call: Call<LoginResponse>,
                            response: Response<LoginResponse>
                        ) {
                            if (response.body()?.status == "success") {

                                SharedPreference.getInstance(applicationContext).saveUser(response.body()?.data!!)
                                val intent = Intent(applicationContext,HomeActivity::class.java)
                                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                                startActivity(intent)
                                Toast.makeText(applicationContext, "Login Success", Toast.LENGTH_LONG).show()

                                hideLoading()
                            } else if (response.body()?.status == "error") {
                                Toast.makeText(applicationContext,
                                    "Login error",
                                    Toast.LENGTH_LONG
                                ).show()
                                hideLoading()
                            } else {
                                Toast.makeText(
                                    applicationContext,
                                    "Some error occurred try again",
                                    Toast.LENGTH_LONG
                                ).show()
                                hideLoading()
                            }

                        }

                        override fun onFailure(call: Call<LoginResponse>, t: Throwable) {
                            Toast.makeText(applicationContext, t.message, Toast.LENGTH_LONG)
                                .show()
                            hideLoading()

                        }


                    })

            }
            else {
                hideLoading()
                Toast.makeText(this, "Connect to internet", Toast.LENGTH_LONG).show()
            }

        }

    }

    override fun onBackPressed() {
        val intent = Intent(Intent.ACTION_MAIN)
        intent.addCategory(Intent.CATEGORY_HOME)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        startActivity(intent)
    }
}

private fun <T> Call<T>.enqueue(callback: Callback<LoginResponse>) {

}
